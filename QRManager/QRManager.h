//
//  QRManager.h
//  QR
//
//  Created by Driss on 26/7/16.
//  Copyright © 2016 Driss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QRManager : NSObject
+ (UIImage *)createQRForString:(NSString *)qrString qrImageName:(NSString*) qrImageName;

@end
