//
//  QRManager.m
//  QR
//
//  Created by Driss on 26/7/16.
//  Copyright © 2016 Driss. All rights reserved.
//

#import "QRManager.h"

@implementation QRManager

+ (UIImage *)createQRForString:(NSString *)qrString qrImageName:(NSString*) qrImageName {
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Size: %zd", stringData.length);
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    
    CIImage * qrCIImage = qrFilter.outputImage;
    
    CIFilter* colorFilter = [CIFilter filterWithName:@"CIFalseColor"];
    [colorFilter setDefaults];
    [colorFilter setValue:qrCIImage forKey:@"inputImage"];
    [colorFilter setValue:[CIColor colorWithRed:0 green:0 blue:0] forKey:@"inputColor0"];
    [colorFilter setValue:[CIColor colorWithRed:1 green:1 blue:1] forKey:@"inputColor1"];
    
    
    UIImage* codeImage = [UIImage imageWithCIImage:[colorFilter.outputImage imageByApplyingTransform:CGAffineTransformMakeScale(5, 5)]];
    UIImage* iconImage = [UIImage imageNamed:qrImageName];
    if(iconImage!=nil) {
        
        CGRect rect = CGRectMake(0, 0, codeImage.size.width, codeImage.size.height);
        UIGraphicsBeginImageContext(rect.size);
        
        [codeImage drawInRect:rect];
        
        CGSize avatarSize = CGSizeMake(rect.size.width * 0.25, rect.size.height * 0.25);
        CGFloat x = (rect.size.width - avatarSize.width) * 0.5;
        CGFloat y = (rect.size.height - avatarSize.height) * 0.5;
        [iconImage drawInRect:CGRectMake(x, y, avatarSize.width, avatarSize.height)];
        
        UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultImage;
        
    }
    
    return codeImage;
    
    
}
/*
- (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale
{
    // Render the CIImage into a CGImage
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    
    // Now we'll rescale using CoreGraphics
    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    CGContextRef context = UIGraphicsGetCurrentContext();
    // We don't want to interpolate (since we've got a pixel-correct image)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    // Get the image out
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Tidy up
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return scaledImage;
}
*/

@end
