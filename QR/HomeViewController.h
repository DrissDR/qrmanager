//
//  HomeViewController.h
//  QR
//
//  Created by Driss on 26/7/16.
//  Copyright © 2016 Driss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
- (IBAction)showGenerarQR:(UIButton *)sender;

- (IBAction)showLeerQR:(UIButton *)sender;
@end
