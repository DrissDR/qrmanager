//
//  HomeViewController.m
//  QR
//
//  Created by Driss on 26/7/16.
//  Copyright © 2016 Driss. All rights reserved.
//

#import "HomeViewController.h"
#import "QRGeneratorViewController.h"
#import "QRManager.h"
#import "AES256Manager.h"

@interface HomeViewController ()
@property (nonatomic, strong) QRGeneratorViewController * generatorViewController;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showGenerarQR:(UIButton *)sender {
    
    
    
    self.generatorViewController = [[QRGeneratorViewController alloc] initWithNibName:@"QRGeneratorViewController" bundle:nil];
    
    NSString * QREncryptedString = [AES256Manager encrypt:@"Drisss WIllisse"];
    
    self.generatorViewController.qrImage = [QRManager createQRForString:QREncryptedString qrImageName:@"logo"];
    [self.navigationController pushViewController:self.generatorViewController animated:YES];
    
}

- (IBAction)showLeerQR:(UIButton *)sender {
}
@end
